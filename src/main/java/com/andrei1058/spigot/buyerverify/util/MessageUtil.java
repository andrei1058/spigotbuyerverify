package com.andrei1058.spigot.buyerverify.util;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import org.javacord.api.entity.Icon;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class MessageUtil {

    public static EmbedBuilder createEmbedCmdList(User user, String header, String desc, List<String> fields, List<String> data, Color color) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setThumbnail(BuyerVerify.getDiscord().getYourself().getAvatar());
        eb.setTitle(header);
        eb.setDescription(desc);
        eb.setColor(color);

        int i = 0;
        for (String field : fields) {
            eb.addField(field, data.get(i));
            i++;
        }

        eb.setFooter("Requested by " + user.getDiscriminatedName(), user.getAvatar());
        return eb;
    }

    public static EmbedBuilder createEmbedList(User user, String header, String desc, List<String> fields, List<String> data, Color color) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setThumbnail(BuyerVerify.getDiscord().getYourself().getAvatar());
        eb.setTitle(header);
        eb.setDescription(desc);
        eb.setColor(color);

        int i = 0;
        for (String field : fields) {
            eb.addField(field, data.get(i), true);
            i++;
        }

        eb.setFooter("Requested by " + user.getDiscriminatedName(), user.getAvatar());
        return eb;
    }

    public static EmbedBuilder createEmbedUsage(MessageAuthor user, String header, String desc, Color color) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setThumbnail(BuyerVerify.getDiscord().getYourself().getAvatar());
        eb.setTitle(header);
        eb.setDescription(desc);
        eb.setColor(color);
        eb.setFooter("Requested by " + user.getDiscriminatedName(), user.getAvatar());
        return eb;
    }

    public static EmbedBuilder createEmbed(MessageAuthor user, String header, String desc, String iconUrl, Color color) {
        return createEmbed(user.asUser().get(), header, desc, iconUrl,color);
    }

    public static EmbedBuilder createEmbed(User user, String header, String desc, String iconUrl, Color color) {
        EmbedBuilder eb = new EmbedBuilder();
        if (iconUrl.isEmpty()) {
            eb.setThumbnail(user.getAvatar());
        } else {
            eb.setThumbnail(iconUrl);
        }
        eb.setTitle(header);
        eb.setDescription(desc);
        eb.setColor(color);
        eb.setFooter("Requested by " + user.getDiscriminatedName(), user.getAvatar());
        return eb;
    }

    public static void scheduleDelete(Message m, int seconds){
        BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, seconds, TimeUnit.SECONDS);
    }
}
