package com.andrei1058.spigot.buyerverify.configuration;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.yamlutil.Configuration;
import com.andrei1058.yamlutil.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class BotConfig {

    private static BotConfig botConfig = null;

    private File file;
    private Configuration yml;

    private BotConfig() throws IOException {
        file = new File("config.yml");

        if (!file.exists()) {
            try {
                if (!file.createNewFile()){
                    BuyerVerify.getLogger().error("Could not create config file. Make sure this user has the write permission.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            yml = YamlConfiguration.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            throw e;
        }

        if (yml.get("token") == null) yml.set("token", "your-token-here");
        if (yml.get("database.host") == null) yml.set("database.host", "localhost");
        if (yml.get("database.port") == null) yml.set("database.port", 3306);
        if (yml.get("database.user") == null) yml.set("database.user", "root");
        if (yml.get("database.pass") == null) yml.set("database.pass", "bread");
        if (yml.get("database.name") == null) yml.set("database.name", "spigotVerify");
        if (yml.get("spigot-account.email") == null) yml.set("spigot-account.email", "verifyBOT");
        if (yml.get("spigot-account.pass") == null) yml.set("spigot-account.pass", "bread");

        try {
            YamlConfiguration.getProvider(YamlConfiguration.class).save(yml, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get bot login token.
     */
    public static String getToken() {
        return botConfig.yml.getString("token");
    }

    public static Configuration getYml() {
        return botConfig.yml;
    }

    /**
     * Initialize bot configuration.
     */
    public static boolean init() {
        if (botConfig == null) {
            try {
                botConfig = new BotConfig();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
