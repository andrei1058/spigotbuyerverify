package com.andrei1058.spigot.buyerverify;

import com.andrei1058.spigot.buyerverify.command.Command;
import com.andrei1058.spigot.buyerverify.database.Database;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class MessageListener implements MessageCreateListener {

    private static HashMap<Long, String> prefix = new HashMap<>();
    private static HashMap<Long, Long> cmdChannels = new HashMap<>();

    private static HashMap<Long, LinkedList<Long>> antiPingUser = new HashMap<>(), antiPingRole = new HashMap<>();

    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        if (event.getMessage().getContent().trim().isEmpty()) return;
        if (!event.isServerMessage()) return;
        long server = event.getServer().get().getId();
        boolean deleted = false;
        if (cmdChannels.containsKey(server) && cmdChannels.get(server) == event.getChannel().getId() && !event.getMessageAuthor().isBotUser()) {
            deleted = true;
            event.getMessage().delete();
        }
        String prefix = getPrefix(server);

        // manage messages
        if (event.getMessage().getContent().startsWith(prefix)) {
            String[] args = event.getMessage().getContent().replaceFirst(prefix, "").split(" ");
            if (args.length == 0) return;

            for (Command c : Command.getCommands()) {
                if (c.getName().equalsIgnoreCase(args[0]) && c.hasPermission(event.getMessageAuthor())) {
                    if (event.getChannel().canYouManageMessages() && !deleted) {
                        event.getMessage().delete();
                    }
                    c.execute(event.getChannel(), event.getMessage().getAuthor(), Arrays.copyOfRange(args, 1, args.length), event.getMessage(), prefix);
                }
            }
        }

        // manage anti ping
        if (event.getMessageAuthor().isBotUser()) return;
        if (!event.getMessage().getMentionedUsers().isEmpty()){
            BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(()-> {
                for (User u : event.getMessage().getMentionedUsers()){
                    long guild = event.getServer().get().getId();
                    if (isAntiPingUser(guild, u.getId())){
                        String message = Database.getDatabase().getAntiPingMsg(guild, u.getId(), "user");
                        event.getChannel().sendMessage(MessageUtil.createEmbed(event.getMessageAuthor(), "NO NO NO!",
                                event.getMessageAuthor().asUser().get().getMentionTag() + "\n" + message, "", Color.RED));
                    }
                }
            });
        }
        if (!event.getMessage().getMentionedRoles().isEmpty()){
            BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(()-> {
                for (Role u : event.getMessage().getMentionedRoles()){
                    long guild = event.getServer().get().getId();
                    if (isAntiPingRole(guild, u.getId())){
                        String message = Database.getDatabase().getAntiPingMsg(guild, u.getId(), "role");
                        event.getChannel().sendMessage(MessageUtil.createEmbed(event.getMessageAuthor(), "NO NO NO!",
                                event.getMessageAuthor().asUser().get().getMentionTag() + "\n" + message, "", Color.RED));
                    }
                }
            });
        }
    }

    public static String getPrefix(Long guild) {
        if (prefix.containsKey(guild)) return prefix.get(guild);
        String p = Database.getDatabase().getSetting(guild, "prefix");
        p = p == null ? "!" : p;
        prefix.put(guild, p);
        return p;
    }

    public static void setPrefix(Long guild, String pref) {
        if (prefix.containsKey(guild)) prefix.replace(guild, pref);
        else prefix.put(guild, pref);
    }

    public static void setChannel(long guild, long channel) {
        if (cmdChannels.containsKey(guild)) cmdChannels.replace(guild, channel);
        else cmdChannels.put(guild, channel);
    }

    public static void addAntiPingUser(long guild, long user) {
        if (!antiPingUser.containsKey(guild)) {
            LinkedList<Long> list = new LinkedList<>();
            list.add(user);
            antiPingUser.put(guild, list);
            return;
        }

        antiPingUser.get(guild).add(user);
    }

    public static void removeAntiPingUser(long guild, long user) {
        if (!antiPingUser.containsKey(guild)) return;
        antiPingUser.get(guild).remove(user);
    }

    public static boolean isAntiPingUser(long guild, long user) {
        if (antiPingUser.containsKey(guild)) return antiPingUser.get(guild).contains(user);
        return false;
    }

    public static void addAntiPingRole(long guild, long user) {
        if (!antiPingRole.containsKey(guild)) {
            LinkedList<Long> list = new LinkedList<>();
            list.add(user);
            antiPingRole.put(guild, list);
            return;
        }

        antiPingRole.get(guild).add(user);
    }

    public static void removeAntiPingRole(long guild, long user) {
        if (!antiPingRole.containsKey(guild)) return;
        antiPingRole.get(guild).remove(user);
    }

    public static boolean isAntiPingRole(long guild, long user) {
        if (antiPingRole.containsKey(guild)) return antiPingRole.get(guild).contains(user);
        return false;
    }
}
