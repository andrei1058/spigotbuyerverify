package com.andrei1058.spigot.buyerverify.database;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.spigot.buyerverify.PendingVerify;
import com.andrei1058.spigot.buyerverify.configuration.BotConfig;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.sql.*;
import java.util.*;

public class Database {

    private static Database database;

    private Connection connection;

    public Database() {
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        if (!connect()) {
            BuyerVerify.getLogger().error("Can't connect to the database!");
            if (BuyerVerify.getDiscord() != null) {
                BuyerVerify.getDiscord().disconnect();
            }
            System.exit(1);
        }

        try {
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS resource_general (guild BIGINT, resource INT);");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS resource_role (guild BIGINT, resource INT, role BIGINT);");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS resource_verified (guild BIGINT, resource INT, user BIGINT, spigot INT);");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS resource_settings (guild BIGINT, resource INT, name VARCHAR(255), value VARCHAR(255));");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS server_settings (guild BIGINT, name VARCHAR(255), value VARCHAR(255));");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS anti_ping (guild BIGINT, id BIGINT, message VARCHAR(255), type VARCHAR(30));");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS pending_verify (guild BIGINT, channel BIGINT, user BIGINT, resource INT, resource_name VARCHAR(255), token VARCHAR(255));");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void setVerified(long guild, int resource, long user, int spigot) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("INSERT INTO resource_verified VALUES ('" + guild + "', '" + resource + "', '" + user + "', '" + spigot + "');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public LinkedList<Long> getRoles(int resource, long guild) {
        if (!isConnected()) connect();
        LinkedList<Long> roles = new LinkedList<>();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT role FROM resource_role WHERE guild = '" + guild + "' AND resource = '" + resource + "';")) {
            while (rs.next()) {
                roles.add(rs.getLong("role"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roles;
    }

    public void setRoles(int resource, long guild, LinkedList<Long> roles) {
        if (!isConnected()) connect();
        try {
            for (long role : roles) {
                connection.createStatement().execute("INSERT INTO resource_role VALUES ('" + guild + "', '" + resource + "', '" + role + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createResource(int resource, long guild, LinkedList<Long> roles) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("INSERT INTO resource_general VALUES ('" + guild + "', '" + resource + "');");
            connection.createStatement().execute("INSERT INTO resource_settings VALUES ('" + guild + "', '" + resource + "', 'max_discord_users_per_spigot_buyer', '1');");
            setRoles(resource, guild, roles);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeResource(int resource, long guild) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("DELETE FROM resource_general WHERE guild='" + guild + "' AND resource='" + resource + "';");
            connection.createStatement().execute("DELETE FROM resource_role WHERE guild='" + guild + "' AND resource='" + resource + "';");
            connection.createStatement().execute("DELETE FROM resource_settings WHERE guild='" + guild + "' AND resource='" + resource + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setMaxUsers(int resource, long guild, int amount) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("UPDATE resource_settings SET value = '" + amount + "' WHERE resource='" + resource + "' AND guild='" + guild + "' AND name='max_discord_users_per_spigot_buyer';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getSetting(long guild, String settingName) {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT value FROM server_settings WHERE guild='" + guild + "' AND name='" + settingName + "';")) {
            if (rs.next()) return rs.getString("value");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<Long, String> getSettings(String settingName) {
        if (!isConnected()) connect();
        HashMap<Long, String> values = new HashMap<>();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT guild, value FROM server_settings WHERE name = '" + settingName + "';")) {
            while (rs.next()) {
                values.put(rs.getLong("guild"), rs.getString("value"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return values;
    }

    public void setSetting(long guild, String settingName, String value) {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT name FROM server_settings WHERE guild='" + guild + "' AND name='" + settingName + "';")) {
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE server_settings SET value = '" + value + "' WHERE guild='" + guild + "' AND name='" + settingName + "';");
            } else {
                try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO server_settings VALUES(?,?,?);")) {
                    preparedStatement.setLong(1, guild);
                    preparedStatement.setString(2, settingName);
                    preparedStatement.setString(3, value);
                    preparedStatement.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getMaxUsers(int resource, long guild) {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT value FROM resource_settings WHERE resource = '" + resource + "' AND guild = '" + guild + "' AND name = 'max_discord_users_per_spigot_buyer';")) {
            if (rs.next()) {
                return Integer.parseInt(rs.getString("value"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public LinkedList<Long> getVerifiedUsers(int resource, long guild) {
        if (!isConnected()) connect();
        LinkedList<Long> users = new LinkedList<>();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT user FROM resource_verified WHERE resource = '" + resource + "' AND guild = '" + guild + "';")) {
            while (rs.next()) {
                users.add(rs.getLong("user"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public LinkedList<Long> getUsersSpigotAssociated(int spigotUserId, long guild) {
        if (!isConnected()) connect();
        LinkedList<Long> list = new LinkedList<>();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT user FROM resource_verified WHERE spigot = '" + spigotUserId + "' AND guild='" + guild + "';")) {
            while (rs.next()) {
                list.add(rs.getLong("user"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public LinkedList<Integer> getResources(long guild) {
        if (!isConnected()) connect();
        LinkedList<Integer> resources = new LinkedList<>();

        try (ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM resource_general WHERE guild='" + guild + "';")) {
            while (rs.next()) {
                resources.add(rs.getInt("resource"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resources;
    }

    public boolean isResourceSet(int resource, long guild) {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT * from resource_general WHERE guild='" + guild + "' AND resource='" + resource + "';")) {
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Long> getGuildAntiPing(long guild, String type) {
        if (!isConnected()) connect();
        List<Long> ids = new ArrayList<>();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM anti_ping WHERE guild = '" + guild + "' AND type='" + type.toLowerCase() + "';")) {
            while (rs.next()) {
                ids.add(rs.getLong("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ids;
    }

    public String getAntiPingMsg(long guild, long id, String type) {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT message FROM anti_ping WHERE guild = '" + guild + "' AND id = '" + id + "' AND type='" + type.toLowerCase() + "';")) {
            if (rs.next()) return new String(Base64.decode(rs.getString("message")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Do not ping this user/ rank!";
    }

    public void addAntiPing(long guild, long id, String message, String type) {
        if (!isConnected()) connect();
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO anti_ping VALUES (?,?,?,?)")) {
            ps.setLong(1, guild);
            ps.setLong(2, id);
            ps.setString(3, Base64.encode(message.getBytes()));
            ps.setString(4, type.toLowerCase());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeAntiPing(long guild, long id, String type) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("DELETE FROM anti_ping WHERE guild = '" + guild + "' AND id ='" + id + "' AND type='" + type + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPending(long guild, long channel, long user, int resource, String resource_name, UUID token) {
        if (!isConnected()) connect();
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO pending_verify VALUES (?,?,?,?,?,?);")) {
            ps.setLong(1, guild);
            ps.setLong(2, channel);
            ps.setLong(3, user);
            ps.setInt(4, resource);
            ps.setString(5, Base64.encode(resource_name.length() > 254 ? resource_name.substring(0, 254).getBytes() : resource_name.getBytes()));
            ps.setString(6, token.toString());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadPending() {
        if (!isConnected()) connect();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM pending_verify;")) {
            while (rs.next()) {
                new PendingVerify(rs.getLong("guild"), rs.getLong("channel"), rs.getLong("user"), rs.getInt("resource"),
                        new String(Base64.decode(rs.getString("resource_name"))), UUID.fromString(rs.getString("token")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removePending(long guild, UUID token) {
        if (!isConnected()) connect();
        try {
            connection.prepareStatement("DELETE FROM pending_verify WHERE guild='" + guild + "' AND token='" + token.toString() + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + BotConfig.getYml().getString("database.host")
                    + ":" + BotConfig.getYml().getInt("database.port")
                    + "/" + BotConfig.getYml().getString("database.name")
                    + "?autoReconnect=true&user=" + BotConfig.getYml().getString("database.user")
                    + "&password=" + BotConfig.getYml().getString("database.pass")
                    + "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isConnected() {
        try {
            return connection != null && connection.isValid(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void init() {
        if (database == null) database = new Database();
    }

    public static Database getDatabase() {
        return database;
    }
}
