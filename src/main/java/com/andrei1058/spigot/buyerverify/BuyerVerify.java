package com.andrei1058.spigot.buyerverify;

import com.andrei1058.spigot.buyerverify.command.Resource;
import com.andrei1058.spigot.buyerverify.command.SpigotMc;
import com.andrei1058.spigot.buyerverify.command.Verify;
import com.andrei1058.spigot.buyerverify.configuration.BotConfig;
import com.andrei1058.spigot.buyerverify.database.Database;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BuyerVerify {

    private static DiscordApi discord;
    private static Logger logger;
    public static final String version = "0.2.1";

    public static void main(String[] args) {
        if (!BotConfig.init()) return;
        Database.init();
        logger = LoggerFactory.getLogger(BuyerVerify.class);
        discord = new DiscordApiBuilder().setToken(BotConfig.getToken()).login().join();
        discord.addMessageCreateListener(new MessageListener());
        discord.updateActivity(ActivityType.WATCHING, "!verify");
        new Resource();
        new Verify();
        new SpigotMc();

        // cache cmd channel
        for (HashMap.Entry<Long, String> e : Database.getDatabase().getSettings("cmd_channel").entrySet()){
            MessageListener.setChannel(e.getKey(), Long.parseLong(e.getValue()));
        }

        // cache anti ping
        for (Server e : discord.getServers()){
            for (Long user : Database.getDatabase().getGuildAntiPing(e.getId(), "user")){
                MessageListener.addAntiPingUser(e.getId(), user);
            }
            for (Long user : Database.getDatabase().getGuildAntiPing(e.getId(), "role")){
                MessageListener.addAntiPingRole(e.getId(), user);
            }
        }

        // load pending verify
        //Database.getDatabase().loadPending();

        //PendingVerify ppv = new PendingVerify(630136028472147990L, 630370092554649621L, 178199397782257665L, 50942, "BedWars1058 - The most modern bedwars plugin. [bungee/multiarena/shared]");
        //ppv.setToken("6147bb01-cc5d-4043-9e95-47bae284a24d");

        discord.getThreadPool().getScheduler().scheduleWithFixedDelay(() -> {
            logger.info("Updating..");
            LinkedList<Integer> resources = new LinkedList<>();
            for (PendingVerify pv : PendingVerify.getPendingVerifies()) {
                if (!resources.contains(pv.getResourceID())) resources.add(pv.getResourceID());
            }

            for (Integer i : resources) {
                try {
                    sendPost(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1, TimeUnit.MINUTES);
    }

    private static void sendPost(int resource) throws Exception {
        logger.info("Scanning resource " + resource);

        String url = "https://www.spigotmc.org/login/login";
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("login", BotConfig.getYml().getString("spigot-account.email")));
        urlParameters.add(new BasicNameValuePair("password", BotConfig.getYml().getString("spigot-account.pass")));
        urlParameters.add(new BasicNameValuePair("cookie_check", "0"));
        urlParameters.add(new BasicNameValuePair("_xfToken", ""));
        urlParameters.add(new BasicNameValuePair("redirect", "https://www.spigotmc.org/resources/" + resource + "/reviews"));
        urlParameters.add(new BasicNameValuePair("remember", "1"));
        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        // add header
        post.setHeader("User-Agent", "Mozilla/5.0");
        HttpResponse response = client.execute(post);

        if (response.getStatusLine().getStatusCode() != 200) {
            logger.warn("Cannot scan " + resource + ". HTTP Response: " + response.getStatusLine().getStatusCode());
        }


        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        String data = result.replaceAll("\\s+", "");
        String[] reviews = data.split("<liid=\"review-");

        LinkedList<String> activeTokens = new LinkedList<>();

        for (int i = reviews.length - 1; i > 0; i--) {

            String[] temp = reviews[i].split("<ahref=\"members/")[1].split("/\"class=\"avatar")[0].split("\\.");
            int userId = Integer.parseInt(temp[temp.length - 1]);

            temp = reviews[i].split("<article><blockquoteclass=\"ugcbaseHtml\">")[1].split("</blockquote></article>");
            for (PendingVerify pv : new ArrayList<>(PendingVerify.getPendingVerifies())) {
                if (pv.getResourceID() == resource) {
                    if (temp[0].contains(pv.getToken())) {
                        if (!pv.isSpigotVerified()) pv.manage(userId);
                        activeTokens.add(pv.getToken());
                    }
                    pv.manageTimeOut();
                }
            }
        }

        for (PendingVerify pv : PendingVerify.getPendingVerifies()) {
            if (!activeTokens.contains(pv.getToken())) {
                //id doesn't count in this case
                if (pv.isSpigotVerified()) pv.manage(0);
            }
        }
    }


    public static Logger getLogger() {
        return logger;
    }

    public static DiscordApi getDiscord() {
        return discord;
    }
}
