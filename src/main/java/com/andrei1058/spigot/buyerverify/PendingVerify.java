package com.andrei1058.spigot.buyerverify;

import com.andrei1058.spigot.buyerverify.database.Database;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import io.CodedByYou.spiget.Author;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class PendingVerify {
    private static LinkedList<PendingVerify> pendingVerifies = new LinkedList<>();

    private long server, channel, sender;
    private int spigotUser, resourceID;
    // if there are multiple tokens in review give to the first one generated
    private UUID token;
    private long message;
    private String resourceName;

    private boolean spigotVerified = false;
    private int timeOut = 10;

    public PendingVerify(long server, long channel, long sender, int resourceID, String resourceName) {
        this.sender = sender;
        this.server = server;
        this.channel = channel;
        this.resourceID = resourceID;

        do {
            this.token = UUID.randomUUID();
        } while (isTokenInUse(token));
        this.resourceName = resourceName;
        pendingVerifies.add(this);
        Database.getDatabase().addPending(sender, channel, sender, resourceID, resourceName, token);
    }

    public PendingVerify(long server, long channel, long sender, int resourceID, String resourceName, UUID token) {
        this.sender = sender;
        this.server = server;
        this.channel = channel;
        this.resourceID = resourceID;
        this.token = token;
        this.resourceName = resourceName;
        pendingVerifies.add(this);
    }

    public void manageTimeOut(){
        if (timeOut == 0){
            pendingVerifies.remove(this);
            Database.getDatabase().removePending(server, token);
            return;
        }
        timeOut--;
    }

    public void manage(int buyerID) {
        if (spigotVerified) {
            //removed, done
            if (BuyerVerify.getDiscord().getServerById(server).isPresent()) {
                Server s = BuyerVerify.getDiscord().getServerById(server).get();
                TextChannel t = s.getTextChannelById(channel).isPresent() ? s.getTextChannelById(channel).get() : null;
                if (t != null) {
                    t.deleteMessages(message);
                    User u = s.getMemberById(sender).isPresent() ? s.getMemberById(sender).orElse(null) : null;
                    if (u != null) {
                        Author a = null;
                        try {
                            a = new Author(this.spigotUser);
                        } catch (Exception ignored) {
                        }
                        int limit = Database.getDatabase().getMaxUsers(resourceID, server);
                        if (Database.getDatabase().getUsersSpigotAssociated(this.spigotUser, server).size() < limit) {
                            for (Long role : Database.getDatabase().getRoles(resourceID, s.getId())) {
                                Role r = s.getRoleById(role).orElse(null);
                                if (s.canManageRole(BuyerVerify.getDiscord().getYourself(), r))
                                    s.addRoleToUser(u, r);
                            }
                            Database.getDatabase().setVerified(s.getId(), resourceID, u.getId(), this.spigotUser);
                            t.sendMessage(u.getMentionTag()).thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 2, TimeUnit.MINUTES));
                            t.sendMessage(MessageUtil.createEmbed(u, "All Done", "Your are now verified as **" + (a != null ? a.getName() : "New user") + "**\n" +
                                    "*" + resourceName + "*", a != null ? a.getIcon() : "", Color.orange)).thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 2, TimeUnit.MINUTES));
                        } else {
                            StringBuilder msg = new StringBuilder("You have reached the limit of " + limit + " allowed Discord users per spigot account.\n");
                            for (Long l : Database.getDatabase().getUsersSpigotAssociated(this.spigotUser, server)) {
                                User u2 = s.getMemberById(l).orElse(null);
                                msg.append(u2 != null ? u2.getMentionTag() : l + "\n");
                            }
                            msg.append("\n").append(resourceName);
                            t.sendMessage(MessageUtil.createEmbed(u, "Error", msg.toString(), a != null ? a.getIcon() : "", Color.red))
                                    .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 2, TimeUnit.MINUTES));

                        }
                    }
                    pendingVerifies.remove(this);
                    Database.getDatabase().removePending(server, token);
                }
            }
            return;
        }

        //review written
        this.spigotUser = buyerID;
        if (BuyerVerify.getDiscord().getServerById(server).isPresent()) {
            Server s = BuyerVerify.getDiscord().getServerById(server).get();
            TextChannel t = s.getTextChannelById(channel).isPresent() ? s.getTextChannelById(channel).get() : null;
            if (t != null) {
                User u = s.getMemberById(sender).isPresent() ? s.getMemberById(sender).orElse(null) : null;
                if (u != null) {
                    Author a = null;
                    try {
                        a = new Author(buyerID);
                    } catch (Exception ignored) {
                    }
                    t.sendMessage(u.getMentionTag()).thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 2, TimeUnit.MINUTES));
                    t.sendMessage(MessageUtil.createEmbed(u, "One Last Step", "Your account was linked: *" + (a != null ? a.getName() : "New user") + "*" +
                            "\n\nTo complete verification please **delete your review**.\nhttps://spigotmc.org/resources/" + resourceID, a != null ? a.getIcon() : "", Color.orange)).thenAccept(m -> message = m.getId());
                    spigotVerified = true;
                    timeOut = 10;
                } else {
                    // user left
                    pendingVerifies.remove(this);
                }
            }
        }
    }

    public boolean isSpigotVerified() {
        return spigotVerified;
    }

    public void setToken(String token) {
        this.token = UUID.fromString(token);
    }

    public int getResourceID() {
        return resourceID;
    }

    public String getToken() {
        return token.toString();
    }

    public long getServer() {
        return server;
    }

    public long getSender() {
        return sender;
    }

    public static Collection<PendingVerify> getPendingVerifies() {
        return Collections.unmodifiableCollection(pendingVerifies);
    }

    public static boolean isTokenInUse(UUID uuid) {
        for (PendingVerify pv : pendingVerifies) {
            if (pv.token.equals(uuid)) return true;
        }
        return false;
    }
}
