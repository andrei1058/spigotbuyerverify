package com.andrei1058.spigot.buyerverify.command;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.spigot.buyerverify.PendingVerify;
import com.andrei1058.spigot.buyerverify.database.Database;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import io.CodedByYou.spiget.Resource;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;

import java.awt.*;

import java.util.LinkedList;

public class Verify extends Command {

    public Verify() {
        super("verify");
    }

    @Override
    public void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
        BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(() -> {
            boolean showUsage = true;
            int resourceId = 0;
            if (args.length != 0) {
                try {
                    resourceId = Integer.parseInt(args[0]);
                    showUsage = false;
                } catch (NumberFormatException ignored) {
                }
            }
            if (showUsage) {

                long serverId = channel.asServerChannel().get().getServer().getId();
                LinkedList<Integer> resources = Database.getDatabase().getResources(serverId);

                for (PendingVerify pv : PendingVerify.getPendingVerifies()) {
                    if (pv.getServer() == serverId && pv.getSender() == sender.getId()) {
                        try {
                            io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(pv.getResourceID());
                            channel.sendMessage(MessageUtil.createEmbed(sender, "Pending Verification", "**" + r.getResourceName() + "**"
                                    + (pv.isSpigotVerified() ? "\nDelete your review to complete verification!" : ""), r.getResourceIconLink(), Color.blue))
                                    .thenAccept(m -> MessageUtil.scheduleDelete(m, 25));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        resources.remove(pv.getResourceID());
                    }
                }

                if (!resources.isEmpty()) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Get verified as a buyer", "Type one of the following commands to get verified.\nExample: !verify 12345", Color.GREEN))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 40));
                }
                for (Integer resource : resources) {
                    String name;
                    io.CodedByYou.spiget.Resource r = null;
                    try {
                        r = new io.CodedByYou.spiget.Resource(resource);
                        name = r.getResourceName();
                    } catch (Exception e) {
                        name = "Something went wrong.";
                    }
                    channel.sendMessage(MessageUtil.createEmbed(sender, prefix + getName() + " " + resource, "Get verified for resource: **" + name + "**", r != null ? r.getResourceIconLink() :
                            BuyerVerify.getDiscord().getYourself().getAvatar().getUrl().getPath(), Color.ORANGE))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 40));
                }
            } else {
                if (!Database.getDatabase().isResourceSet(resourceId, channel.asServerChannel().get().getServer().getId())) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", "There isn't any resource with id **" + resourceId + "**", Color.red))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 25));
                    return;
                }

                long serverId = channel.asServerChannel().get().getServer().getId(), senderId = sender.getId();

                if (Database.getDatabase().getVerifiedUsers(resourceId, serverId).contains(senderId)){
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", "You're already a verified buyer", Color.red))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 25));
                    return;
                }

                for (PendingVerify pv : PendingVerify.getPendingVerifies()) {
                    if (pv.getServer() == serverId && pv.getSender() == sender.getId()) {
                        if (pv.getResourceID() == resourceId) {
                            Resource r = null;
                            try {
                                r = new Resource(pv.getResourceID());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            channel.sendMessage(MessageUtil.createEmbed(sender, "Error!", "Verification for **" + args[0] + "** is already in pending!" +
                                    "\n\nPlease write a review here: \nhttps://spigotmc.org/resources/" + pv.getResourceID() +
                                    "\n\n and **include** this token in your message: \n*" + pv.getToken() + "*" +
                                    "\n \nIt may take a few minutes. Be patient!", r != null ? r.getResourceIconLink() : BuyerVerify.getDiscord().getYourself().getAvatar().getUrl().getPath(), Color.red))
                                    .thenAccept(m -> MessageUtil.scheduleDelete(m, 60));
                            return;
                        }
                    }
                }

                Resource r = null;
                try {
                    r = new Resource(resourceId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (r != null) {
                    PendingVerify pv = new PendingVerify(channel.asServerChannel().get().getServer().getId(), channel.asServerChannel().get().getId(), sender.asUser().get().getId(), resourceId, r.getResourceName());
                    channel.sendMessage(MessageUtil.createEmbed(sender, "Next Step", "In order to get verified for **" + resourceId + "** complete this task!" +
                            "\n\nPlease write a review here: \nhttps://spigotmc.org/resources/" + resourceId +
                            "\n\n and **include** this token in your message: \n*" + pv.getToken() + "*" +
                            "\n\nOr just copy paste this:\n" +
                            "```This is a buyer verification.\nI will delete my review once I got verified.\n" + pv.getToken() + "```" +
                            "\n \nIt may take a few minutes. Be patient!", r.getResourceIconLink(), Color.orange))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 120));
                } else {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", "Resource **" + resourceId + "** is not reachable at the moment.", Color.red))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 25));
                }
            }
        });
    }

    @Override
    public boolean hasPermission(MessageAuthor sender) {
        return true;
    }
}
