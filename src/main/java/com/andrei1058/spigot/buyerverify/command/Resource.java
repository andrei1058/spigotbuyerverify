package com.andrei1058.spigot.buyerverify.command;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.spigot.buyerverify.database.Database;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Resource extends Command {

    public Resource() {
        super("resource");

        registerSubCommands(new SubCommand("add", "Add a new spigot resource.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                if (args.length < 2) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <roleID> <otherRoles>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                int resource;
                try {
                    resource = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <roleID> <otherRoles>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                try {
                    Long.parseLong(args[1]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <roleID> <otherRoles>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(() -> {
                    LinkedList<Long> roles = new LinkedList<>();
                    String[] args2 = Arrays.copyOfRange(args, 1, args.length);
                    for (String s : args2) {
                        try {
                            long i = Long.parseLong(s);
                            roles.add(i);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }

                    if (roles.isEmpty()) {
                        channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <roleID> <otherRoles>", Color.red)).
                                thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                        return;
                    }

                    String name = "";
                    try {
                        io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(Integer.parseInt(args[0]));
                        name = r.getResourceName();
                    } catch (Exception ignored) {
                    }

                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", "Resource **" + (name.isEmpty() ? args[0] : name) + "** was added!", Color.ORANGE))
                            .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    Database.getDatabase().createResource(resource, channel.asServerChannel().get().getServer().getId(), roles);
                });
            }
        }, new SubCommand("remove", "Remove a resource from the list.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                if (args.length != 1) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                try {
                    Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                if (!Database.getDatabase().isResourceSet(Integer.parseInt(args[0]), channel.asServerChannel().get().getServer().getId())) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                Database.getDatabase().removeResource(Integer.parseInt(args[0]), channel.asServerChannel().get().getServer().getId());

                String name = "";
                try {
                    io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(Integer.parseInt(args[0]));
                    name = r.getResourceName();
                } catch (Exception ignored) {
                }
                channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", "Resource **" + (name.isEmpty() ? args[0] : name) + "** configuration removed.", Color.ORANGE)).
                        thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
            }
        }, new SubCommand("list", "Show configured resources list.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(() -> {
                    List<String> ids = new ArrayList<>(), names = new ArrayList<>();
                    for (Integer s : Database.getDatabase().getResources(channel.asServerChannel().get().getServer().getId())) {
                        ids.add(String.valueOf(s));
                        try {
                            io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(s);
                            names.add(r.getResourceName());
                        } catch (Exception e) {
                            names.add("N/A");
                        }
                    }
                    sender.asUser().ifPresent(u -> channel.sendMessage(MessageUtil.createEmbedCmdList(u, "Configured resources", "", ids, names, Color.ORANGE)).thenAccept(
                            m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS)));
                });
            }
        }, new SubCommand("setRoles", "Set roles for existing resource.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                if (args.length < 3) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <boolean> <roles> " +
                            "\n \nboolean _true_ gives new roles to already verified users.", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                int resource;
                try {
                    resource = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <boolean> <roles> " +
                            "\n \nboolean _true_ gives new roles to already verified users.", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                boolean giveRoles;
                try {
                    giveRoles = Boolean.parseBoolean(args[1]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <boolean> <roles> " +
                            "\n \nboolean _true_ gives new roles to already verified users.", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(() -> {
                    LinkedList<Long> roles = new LinkedList<>();
                    String[] args2 = Arrays.copyOfRange(args, 3, args.length);
                    for (String s : args2) {
                        try {
                            long i = Long.parseLong(s);
                            roles.add(i);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }

                    if (roles.isEmpty()) {
                        channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <boolean> <roles> " +
                                "\n \nboolean _true_ gives new roles to already verified users.", Color.red)).
                                thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                        return;
                    }

                    Server server = channel.asServerChannel().get().getServer();
                    if (giveRoles) {
                        for (Long role : roles) {
                            Role r = server.getRoleById(role).orElse(null);
                            if (r != null) {
                                if (server.canManageRole(BuyerVerify.getDiscord().getYourself(), r)) {
                                    for (Long user : Database.getDatabase().getVerifiedUsers(resource, server.getId())) {
                                        User u;
                                        try {
                                            u = BuyerVerify.getDiscord().getUserById(user).get();
                                        } catch (InterruptedException | ExecutionException e) {
                                            continue;
                                        }

                                        r.addUser(u);
                                    }
                                }
                            }
                        }
                    }

                    String name = "";
                    try {
                        io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(Integer.parseInt(args[0]));
                        name = r.getResourceName();
                    } catch (Exception ignored) {
                    }

                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", "Roles for resource **" + (name.isEmpty() ? args[0] : name) + "** were updated!", Color.ORANGE))
                            .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    Database.getDatabase().setRoles(resource, channel.asServerChannel().get().getServer().getId(), roles);
                });
            }
        }, new SubCommand("setMaxUsers", "Set amount of allowed Discord users per spigot account.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                if (args.length != 2) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <value>", Color.red))
                            .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                try {
                    Integer.parseInt(args[0]);
                    Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + Resource.this.getName() + " " + this.getName() + " <resourceID> <value>", Color.red))
                            .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }
                long guild = channel.asServerChannel().get().getServer().getId();
                if (!Database.getDatabase().isResourceSet(Integer.parseInt(args[0]), guild)) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", "You didn't configure any resource with ID: " + args[0], Color.red))
                            .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                String name = "";
                try {
                    io.CodedByYou.spiget.Resource r = new io.CodedByYou.spiget.Resource(Integer.parseInt(args[0]));
                    name = r.getResourceName();
                } catch (Exception ignored) {
                }

                channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!",
                        "Maximum amount of Discord accounts per spigot user was set to **" + args[1] + "** for resource **" + (name.isEmpty() ? args[0] : name) + "**", Color.ORANGE))
                        .thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                Database.getDatabase().setMaxUsers(Integer.parseInt(args[0]), guild, Integer.parseInt(args[1]));
            }
        });
    }

    @Override
    public boolean hasPermission(MessageAuthor sender) {
        return sender.isServerAdmin();
    }
}
