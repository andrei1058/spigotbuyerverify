package com.andrei1058.spigot.buyerverify.command;

import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.spigot.buyerverify.MessageListener;
import com.andrei1058.spigot.buyerverify.database.Database;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import org.javacord.api.entity.Mentionable;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.server.Server;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class SpigotMc extends Command {
    public SpigotMc() {
        super("smc");
        registerSubCommands(new SubCommand("setCmdChannel", "Assign a channel to bot commands. Messages will be deleted.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                //todo prevent injection
                if (args.length != 1 || !args[0].equalsIgnoreCase("this")) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + SpigotMc.this.getName() + " " + this.getName() + " this", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }
                long server = channel.asServerChannel().get().getServer().getId();
                channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", "Channel marked as commands channel. ", Color.orange)).
                        thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                Database.getDatabase().setSetting(server, "cmd_channel", channel.getIdAsString());
                MessageListener.setChannel(server, channel.getId());
            }
        }, new SubCommand("setPrefix", "Change bot commands prefix on your server.", getName()) {
            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                //todo prevent injection
                if (args.length != 1) {
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Command usage:", prefix + SpigotMc.this.getName() + " " + this.getName() + " <prefix>", Color.red)).
                            thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                    return;
                }

                long server = channel.asServerChannel().get().getServer().getId();
                channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", "Prefix changed to: " + args[0], Color.orange)).
                        thenAccept(m -> BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) m::delete, 25, TimeUnit.SECONDS));
                Database.getDatabase().setSetting(server, "prefix", args[0]);
                MessageListener.setPrefix(server, args[0]);
            }
        }, new SubCommand("antiPing", "Warn users pinging a listed member.", getName()) {

            @Override
            protected void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
                if (args.length == 0) {
                    sendUsage(channel, sender);
                    return;
                }

                if (args[0].startsWith("add")) {
                    String type = args[0].equalsIgnoreCase("addUser") ? args[0].replaceFirst("add", "") :
                            args[0].equalsIgnoreCase("addRole") ? args[0].replaceFirst("add", "") : null;

                    if (type == null) {
                        sendUsage(channel, sender);
                        return;
                    }

                    if (args.length < 2) {
                        sendSubUsage(channel, sender, "addRole <id> <message>");
                        sendSubUsage(channel, sender, "addUser <id> <message>");
                        return;
                    }

                    long id;
                    try {
                        id = Long.parseLong(args[1]);
                    } catch (Exception ex) {
                        sendSubUsage(channel, sender, "addRole <id> <message>");
                        sendSubUsage(channel, sender, "addUser <id> <message>");
                        return;
                    }

                    long guild = channel.asServerChannel().get().getServer().getId();
                    if (MessageListener.isAntiPingUser(guild, id)) {
                        channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", args[1] + " is already in the list!", Color.RED))
                                .thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
                        return;
                    }

                    if (args.length < 5) {
                        sendSubUsage(channel, sender, args[0] + " <id> Write your message here");
                        return;
                    }

                    String msg = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                    if (msg.length() > 254) {
                        sendSubUsage(channel, sender, getName() + " <id> <message>\n\nYour message is too big. Limit is 254 characters.");
                        return;
                    }

                    Server s = channel.asServerChannel().get().getServer();
                    Mentionable e = type.equalsIgnoreCase("user") ? s.getMemberById(id).orElse(null) : s.getRoleById(id).orElse(null);
                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", (e != null ? e.getMentionTag() : args[0]) + " was added!", Color.ORANGE))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
                    Database.getDatabase().addAntiPing(guild, id, msg, type);

                    if (type.equalsIgnoreCase("user")) MessageListener.addAntiPingUser(guild, id);
                    else MessageListener.addAntiPingRole(guild, id);

                } else if (args[0].equalsIgnoreCase("list")) {

                    Server s = channel.asServerChannel().get().getServer();
                    for (String type : new String[]{"user", "role"}) {
                        List<Long> data = Database.getDatabase().getGuildAntiPing(channel.asServerChannel().get().getServer().getId(), type);
                        if (data.isEmpty()) {
                            channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Anti Ping " + type + " List", "List is empty.", Color.ORANGE));
                            return;
                        }

                        List<String> names = new ArrayList<>(), ids = new ArrayList<>();
                        for (Long l : data) {
                            ids.add(String.valueOf(l));
                            Mentionable e = type.equalsIgnoreCase("user") ? s.getMemberById(l).orElse(null) : s.getRoleById(l).orElse(null);
                            names.add(e == null ? String.valueOf(l) : e.getMentionTag());
                        }

                        channel.sendMessage(MessageUtil.createEmbedCmdList(sender.asUser().get(), "Anti Ping " + type + " List", "Users are warned when pinging those " + type + "s.", ids, names, Color.ORANGE))
                                .thenAccept(m -> MessageUtil.scheduleDelete(m, 60));
                    }
                } else if (args[0].startsWith("remove")) {
                    String type = args[0].equalsIgnoreCase("removeUser") ? args[0].replaceFirst("remove", "") :
                            args[0].equalsIgnoreCase("removeRole") ? args[0].replaceFirst("remove", "") : null;

                    if (type == null) {
                        sendUsage(channel, sender);
                        return;
                    }

                    if (args.length != 2) {
                        sendSubUsage(channel, sender, "removeUser <id>");
                        sendSubUsage(channel, sender, "removeRole <id>");
                        return;
                    }

                    long user;
                    try {
                        user = Long.parseLong(args[1]);
                    } catch (Exception ex) {
                        sendSubUsage(channel, sender, "removeUser <id>");
                        sendSubUsage(channel, sender, "removeRole <id>");
                        return;
                    }

                    long guild = channel.asServerChannel().get().getServer().getId();

                    if (type.equalsIgnoreCase("removeUser")) {
                        if (!MessageListener.isAntiPingUser(guild, user)) {
                            channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", user + " is not in the list!", Color.RED))
                                    .thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
                            return;
                        }
                        MessageListener.removeAntiPingUser(guild, user);
                    }
                    if (type.equalsIgnoreCase("removeRole")) {
                        if (!MessageListener.isAntiPingRole(guild, user)) {
                            channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Error!", user + " is not in the list!", Color.RED))
                                    .thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
                            return;
                        }
                        MessageListener.removeAntiPingRole(guild, user);
                    }

                    channel.sendMessage(MessageUtil.createEmbedUsage(sender, "Done!", user + " was removed!", Color.ORANGE))
                            .thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
                    Database.getDatabase().removeAntiPing(guild, user, type);
                } else sendUsage(channel, sender);
            }

            void sendUsage(TextChannel tc, MessageAuthor ma) {
                tc.sendMessage(MessageUtil.createEmbedCmdList(ma.asUser().get(), "Commands List", "v" + BuyerVerify.version,
                        Arrays.asList(SpigotMc.this.getName() + " " + getName() + " addUser <id> <message>",
                                SpigotMc.this.getName() + " " + getName() + " addRole <id> <message>", SpigotMc.this.getName() + " " + getName() + " list", SpigotMc.this.getName() + " " + getName() + " remove <roleId>"),
                        Arrays.asList("War members when mentioning this user.", "War users when mentioning this role.", "See anti-ping roles list.", "Remove a role from the anti-ping list."), Color.ORANGE)).thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
            }

            void sendSubUsage(TextChannel tc, MessageAuthor ma, String sub) {
                tc.sendMessage(MessageUtil.createEmbedUsage(ma, "Usage:", SpigotMc.this.getName() + " " + getName() + " " + sub, Color.RED)).thenAccept(m -> MessageUtil.scheduleDelete(m, 30));
            }
        });
    }

    @Override
    public boolean hasPermission(MessageAuthor sender) {
        return sender.isServerAdmin();
    }
}
