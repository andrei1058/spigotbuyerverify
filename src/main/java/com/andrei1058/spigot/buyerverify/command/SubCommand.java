package com.andrei1058.spigot.buyerverify.command;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;

public abstract class SubCommand {

    private String name, description, parentName;

    public SubCommand(String name, String description, String parentName) {
        this.name = name;
        this.description = description;
        this.parentName = parentName;
    }

    protected abstract void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix);

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getFullName() {
        return parentName + " " + getName();
    }
}

