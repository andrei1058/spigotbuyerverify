package com.andrei1058.spigot.buyerverify.command;


import com.andrei1058.spigot.buyerverify.BuyerVerify;
import com.andrei1058.spigot.buyerverify.util.MessageUtil;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageAuthor;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public abstract class Command {

    private static LinkedList<Command> commands = new LinkedList<>();

    private String name;
    private LinkedList<SubCommand> subCommands = new LinkedList<>();

    public Command(String name) {
        this.name = name;
        commands.add(this);
    }

    public void execute(TextChannel channel, MessageAuthor sender, String[] args, Message message, String prefix) {
        if (args.length == 0) {
            defaultMessage(channel, sender, prefix);
            return;
        }

        boolean found = false;
        for (SubCommand c : subCommands) {
            if (c.getName().equalsIgnoreCase(args[0])) {
                c.execute(channel, sender, Arrays.copyOfRange(args, 1, args.length), message, prefix);
                found = true;
                break;
            }
        }

        if (!found) {
            defaultMessage(channel, sender, prefix);
        }
    }

    public void defaultMessage(TextChannel channel, MessageAuthor member, String prefix) {
        if (member.asUser().isPresent() && channel.canEmbedLinks(member.asUser().get())) {
            List<String> field = new ArrayList<>(), data = new ArrayList<>();
            for (SubCommand sc : subCommands) {
                field.add(prefix + getName() + " " + sc.getName());
                data.add(sc.getDescription());
            }
            BuyerVerify.getDiscord().getThreadPool().getScheduler().execute(() -> {
                //noinspection OptionalGetWithoutIsPresent
                Message message = channel.sendMessage(MessageUtil.createEmbedCmdList(member.asUser().get(), "Commands List", "", field, data, Color.ORANGE)).join();
                BuyerVerify.getDiscord().getThreadPool().getScheduler().schedule((Callable<CompletableFuture<Void>>) message::delete, 25, TimeUnit.SECONDS);
            });
            //MessageAction ac = channel.sendMessage(EmbedMessage.createEmbed(member, "NetherPortal Commands", "", field, data));
            //ac.queue(response -> response.delete().submitAfter(30, TimeUnit.SECONDS));
        }
    }

    public String getName() {
        return name;
    }

    public void registerSubCommands(SubCommand... subCommands) {
        this.subCommands.addAll(Arrays.asList(subCommands));
    }

    public LinkedList<SubCommand> getSubCommands() {
        return subCommands;
    }

    public static LinkedList<Command> getCommands() {
        return commands;
    }

    public abstract boolean hasPermission(MessageAuthor sender);
}

